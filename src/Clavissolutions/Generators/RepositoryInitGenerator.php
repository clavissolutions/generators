<?php   namespace Clavissolutions\Generators;


class RepositoryInitGenerator extends FileTemplateGenerator{

	public function  make($name,$saveTo,array $tokens, array $input){

		if(!$this->file->exists("{$saveTo}/repositories/eloquent"))
			$this->file->makeDirectory("{$saveTo}/repositories/eloquent",0755,true);
		
		$this->getTemplate('AbstractRepository');
		parent::make($name, "{$saveTo}/repositories/AbstractRepository.php",$this->getTokens(),$input);

		$this->getTemplate('RepositoryServiceProvider');
		parent::make($name, "{$saveTo}/repositories/RepositoryServiceProvider.php",$this->getTokens(),$input);

		$this->getTemplate('EloquentRepositoryAbstract');
		parent::make($name, "{$saveTo}/repositories/eloquent/EloquentRepositoryAbstract.php",$this->getTokens(),$input);

		$this->getTemplate('BaseRepositoryInterface');
		parent::make($name, "{$saveTo}/repositories/RepositoryInterface.php",$this->getTokens(),$input);
	}

	function getTemplate($name)
	{
		$this->template = $this->file->get(__DIR__."/templates/{$name}.tpl");
	}

	public function getTokens()
	{
		return array();
	}
}