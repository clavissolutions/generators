<?php   namespace Clavissolutions\Generators;


class RepositoryGenerator extends FileTemplateGenerator{

    public function  make($name,$saveTo,array $tokens, array $input){
        //Make the Repository interface
        $this->getTemplate('repositoryinterface');
        parent::make($name, "{$saveTo}/{$name}RepositoryInterface.php",$this->getTokens(),$input);

        //Make the Repository Class itself
        $this->getTemplate('EloquentRepositoryClass');
        parent::make($name, "{$saveTo}/eloquent/{$name}Repository.php",$this->getTokens(),$input);
    }

    function getTemplate($name)
    {
       $this->template = $this->file->get(__DIR__."/templates/{$name}.tpl");
    }

    public function getTokens()
    {
        return array("name");
    }
}