<?php namespace Clavissolutions\Generators\Commands;
use Clavissolutions\Generators\RepositoryGenerator;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RepositoryStructureCreator extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generate:repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the repository structure for a Model';

    /**
     * Generator used by files
     *
     * @var \Clavissolutions\Generators\RepositoryGenerator
     */
    protected $generator;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RepositoryGenerator $generator)
    {
        parent::__construct();
        $this->generator = $generator;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $input = array();
        $name = $input['name'] = $this->argument('name');
        $path = $this->option('repopath');

        $this->generator->make($name,$path,$this->generator->getTokens(),$input);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('name', InputArgument::REQUIRED, 'The name of the new resource'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('repopath', 'rp', InputOption::VALUE_OPTIONAL, 'The path for the repository folder', 'app/repositories'),
        );
    }

}