<?php namespace Clavissolutions\Generators\Commands;
use Clavissolutions\Generators\ResourceStackGenerator;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ResourceStackCreator extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generate:resource:stack';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a resource stack(controller,model,repository)';

    /**
     * Generator used by files
     *
     * @var \Clavissolutions\Generators\ResourceStackGenerator
     */
    protected $generator;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ResourceStackGenerator $generator)
    {
        parent::__construct();
        $this->generator = $generator;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $name = $input['name'] = $this->argument('name');
        $path = $this->option('path');

        $this->generator->make($name,$path,$this->generator->getTokens(),$input);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('name', InputArgument::REQUIRED, 'The name of the new resource'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('path', 'p', InputOption::VALUE_OPTIONAL, 'The path for app folder', 'app'),
        );
    }

}