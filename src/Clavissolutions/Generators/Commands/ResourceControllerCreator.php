<?php namespace Clavissolutions\Generators\Commands;
use Clavissolutions\Generators\ResourceControllerGenerator;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ResourceControllerCreator extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generate:resource:controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a resource controller for the REST service';

    /**
     * Generator used by files
     *
     * @var \Clavissolutions\Generators\ResourceControllerGenerator
     */
    protected $generator;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ResourceControllerGenerator $generator)
    {
        parent::__construct();
        $this->generator = $generator;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $name = $input['name'] = $this->argument('name');
        $path = $this->option('path');

        $this->generator->make($name,$path,$this->generator->getTokens(),$input);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('name', InputArgument::REQUIRED, 'The name of the new resource'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('path', 'p', InputOption::VALUE_OPTIONAL, 'The path for the controller folder', 'app/controllers'),
        );
    }

}