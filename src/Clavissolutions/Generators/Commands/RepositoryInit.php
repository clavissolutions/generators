<?php namespace Clavissolutions\Generators\Commands;
use Clavissolutions\Generators\RepositoryInitGenerator;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RepositoryInit extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generate:repository:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init repository structure';

    /**
     * Generator used by files
     *
     * @var \Clavissolutions\Generators\RepositoryInitGenerator
     */
    protected $generator;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RepositoryInitGenerator $generator)
    {
        parent::__construct();
        $this->generator = $generator;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $path = $this->option('path');
        $this->generator->make(null,$path,$this->generator->getTokens(),array());
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('path', 'p', InputOption::VALUE_OPTIONAL, 'The path for app folder', 'app'),
        );
    }

}