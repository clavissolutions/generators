<?php

namespace Repositories\Eloquent;

use Illuminate\Support\MessageBag;
use Repositories\{{name}}RepositoryInterface;
use Repositories\Eloquent\EloquentRepositoryAbstract;
use {{name}};

class {{name}}Repository extends EloquentRepositoryAbstract implements {{name}}RepositoryInterface
{
    
    
    public function __construct({{name}} $model)
    {
        $this->model   = $model;
        $this->errors = new MessageBag();
    }

    /**
     * save an instance of {{name}} with new input
     *
     *
     * @param  {{name}} $model
     * @param  array    $input
     * @return bool
     */
    public function saveModel($model, $input)
    {
        //TODO: Implement {{name}}Repository saveModel method
    }

    /**
     * Get All the records
     *
     * @return Collection
     */
    public function all()
    {
        //TODO: Implement {{name}}Repository all method
    }

    /**
     * Get a  record by id
     *
     * @parem string|int|array $id of the record
     * @return {{name}}
     */
    public function find($id)
    {
        //TODO: Implement {{name}}Repository find method
    }

    /**
     * create a new {{name}} from $input
     *
     * If errors occured during model validation
     * they can be retried from errors properpty
     *
     * @param  array $input
     * @return bool
     */
    public function create($input)
    {
        $model = new {{name}}();
        return $this->saveModel($model, $input);
    }

    /**
     * update an existing {{name}}
     *
     * If errors occured during model validation
     * they can be retried from errors properpty
     *
     * @param  int $id
     * @param  array $input
     * @return bool
     */
    public function update($id, $input)
    {
     $model = $this->model->find($id);
        return $this->saveModel($model, $input);
    }

    /**
     * Destroy a record by $id
     *
     * @param string|int|array $id of the record
     * @return {{name}}
     */
    public function destroy($id)
    {
        //TODO: Implement {{name}}Repository destroy method
    }

    /**
    * Get instance to array
    *
    * @return array
    */
    public function toArray()
    {
        //TODO: Implement {{name}}Repository toArray method
    }

    /**
    * Check if errors occured in our last opperation
    *
    * @return bool
    */
    public function hasErrors()
    {
        return count($this->errors) != 0;
    }

    /**
    * Return a key|pair of arrays
    *
    * @return array
    */
    public function getErrors()
    {
        return $this->errors->toArray();
    }


    /**
    * Get the http response code for the last operation
    *
    * @return int
    */
    public function getResponseCode()
    {
        return $this->responserCode;
    }

}