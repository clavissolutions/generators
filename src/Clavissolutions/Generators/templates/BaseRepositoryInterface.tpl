<?php

namespace Repositories;


interface RepositoryInterface {

    /**
     * Get All the records
     *
     * @return Collection
     */
    public function all();

    /**
     * Get a record by id
     *
     * @param string|int|array $id of the record
     * @return Eloquent
     */
    public function find($id);

    /**
     * create a new Eloquent from $input
     *
     * If errors occured during model validation
     * they can be retried from errors properpty
     *
     * @param  array $input
     * @return bool
     */
    public function create($input);


    /**
     * update an existing Model
     *
     * If errors occured during model validation
     * they can be retried from errors properpty
     *
     * @param  int $id
     * @param  array $input
     * @return bool
     */
    public function update($id,$input);

    /**
     * Destroy a record by $id
     *
     * @param string|int|array $id of the record
     * @return bool
     */
    public function destroy($id);

    /**
     * Save an instance of Eloquent with new input
     *
     *
     * @param  Eloquent $model
     * @param  array    $input
     * @return bool
     */
    public function saveModel($model,$input);

    /**
     * Get instance to array
     *
     * @return array
     */
    public function toArray();


    /**
     * Check if errors occured in our last opperation
     *
     * @return bool
     */
    public function hasErrors();

    /**
     * Return a key|pair of arrays
     *
     * @return array
     */
    public function getErrors();

}