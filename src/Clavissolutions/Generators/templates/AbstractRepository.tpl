<?php

namespace Repositories;

use Illuminate\Support\MessageBag;

abstract class AbstractRepository {

    /**
     * @var MessageBag
     */
    protected   $errors;

    /**
     * @var int
     */
    protected $responserCode = 200;


    abstract  public function saveModel($model, $input);
    abstract  public function getResponseCode();

}