<?php namespace Repositories\Eloquent;

use Repositories\AbstractRepository;
use Repositories\RepositoryInterface;


abstract class EloquentRepositoryAbstract extends AbstractRepository implements  RepositoryInterface
{

    /**
     * Get All the records
     *
     * @return Collection
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
    * Get a record by id
    *
    * @param string|int|array $id of the record
    * @return Eloquent
    */
    public function find($id)
    {
        try{
            return $this->model->findOrFail($id);
        }catch (NotFoundException $e){
            $this->errors->add("model","not found");
            $this->responserCode = 404;
            return null;
        }
    }

    /**
    * create a new Eloquent from $input
    *
    * If errors occured during model validation
    * they can be retried from errors properpty
    *
    * @param  array $input
    * @return bool
    */
    public function create($input)
    {
        $model = new $this->model();
        return $this->saveModel($model, $input);
    }

    /**
    * update an existing Eloquent
    *
    * If errors occured during model validation
    * they can be retried from errors properpty
    *
    * @param  int $id
    * @param  array $input
    * @return bool
    */
    public function update($id, $input)
    {
        $model = $this->model->find($id);
        return $this->saveModel($model, $input);
    }


    /**
    * Destroy a record by $id
    *
    * @param string|int|array $id of the record
    * @return bool
    */
    public function  destroy($id){
        $model  = $this->model->find($id);
        if($model === null){
            return false;
        }
        $model->delete();
        return true;
    }

    /**
    * Get instance to array
    *
    * @return array
    */
    public function toArray()
    {
            //TODO: implement to array
    }

    /**
    * Check if errors occured in our last opperation
    *
    * @return bool
    */
    public function hasErrors()
    {
         return count($this->errors) != 0;
    }

    /**
    * Return a key|pair of arrays
    *
    * @return array
    */
    public function getErrors()
    {
        return $this->errors->toArray();
    }


    /**
    * Get the http response code for the last operation
    *
    * @return int
    */
    public function getResponseCode()
    {
        return $this->responserCode;
    }


    public function saveModel($model,$input){
        //TODO: Implement save model
    }

}