<?php

namespace Controllers\Admin;

use Controllers\AbstractResourceController;
use Repositories\{{name}}RepositoryInterface as {{name}};

class {{name}}ResourceController extends AbstractResourceController  {

    /**
     * @var Repositories\{{name}}RepositoryInterface
     */
    protected $model;

    public function __construct({{name}} ${{name}})
    {
        $this->model = ${{name}};
    }

}