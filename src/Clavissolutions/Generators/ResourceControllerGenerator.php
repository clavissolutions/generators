<?php   namespace Clavissolutions\Generators;


class ResourceControllerGenerator extends FileTemplateGenerator{

    public function  make($name,$saveTo,array $tokens, array $input){
        //Make the Repository interface
        $this->getTemplate('resourceController');
        parent::make($name, "{$saveTo}/{$name}ResourceController.php",$this->getTokens(),$input);

    }

    function getTemplate($name)
    {
        $this->template = $this->file->get(__DIR__."/templates/{$name}.tpl");
    }

    public function getTokens()
    {
        return array("name");
    }
}