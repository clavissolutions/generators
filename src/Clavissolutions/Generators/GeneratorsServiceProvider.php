<?php namespace Clavissolutions\Generators;

use Clavissolutions\Generators\Commands\ResourceControllerCreator;
use Illuminate\Support\ServiceProvider;



class GeneratorsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->registerCommands();

        $this->commands(
                     'generate.repository',
                     'generate.resource.controller',
	                 'generate.resource.stack',
	                 'generate.repository.init'
         );
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}


    protected function registerCommands()
     {
         $this->app['generate.repository'] = $this->app->share(function($app)
         {
             $generator = new RepositoryGenerator($app['files']);
             return new Commands\RepositoryStructureCreator($generator);
         });

         $this->app['generate.resource.controller'] = $this->app->share(function($app)
         {
             $generator = new ResourceControllerGenerator($app['files']);
             return new Commands\ResourceControllerCreator($generator);
         });

	     $this->app['generate.resource.stack'] = $this->app->share(function($app)
	     {
		     $generator = new ResourceStackGenerator($app['files']);
		     return new Commands\ResourceStackCreator($generator);
	     });

	     $this->app['generate.repository.init'] = $this->app->share(function($app)
	     {
		     $generator = new RepositoryInitGenerator($app['files']);
		     return new Commands\RepositoryInit($generator);
	     });
     }
}