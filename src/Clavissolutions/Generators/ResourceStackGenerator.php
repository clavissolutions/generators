<?php   namespace Clavissolutions\Generators;


class ResourceStackGenerator extends FileTemplateGenerator{

    public function  make($name,$saveTo,array $tokens, array $input){

        //Make the Repository interface
        $this->getTemplate('resourceController');
	    parent::make($name, "{$saveTo}/controllers/{$name}ResourceController.php",$this->getTokens(),$input);

	    //Make the Repository interface
	    $this->getTemplate('Model');
	    parent::make($name, "{$saveTo}/models/{$name}.php",$this->getTokens(),$input);

	    //Make the Repository Class itself
	    $this->getTemplate('EloquentRepositoryClass');
	    parent::make($name, "{$saveTo}/repositories/eloquent/{$name}Repository.php",$this->getTokens(),$input);


	    //Make the Model Class itself
	    $this->getTemplate('EloquentRepositoryClass');
	    parent::make($name, "{$saveTo}/repositories/eloquent/{$name}Repository.php",$this->getTokens(),$input);

    }

    function getTemplate($name)
    {
        $this->template = $this->file->get(__DIR__."/templates/{$name}.tpl");
    }

    public function getTokens()
    {
        return array("name");
    }
}