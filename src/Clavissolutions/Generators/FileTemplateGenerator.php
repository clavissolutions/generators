<?php  namespace Clavissolutions\Generators;



use Illuminate\Filesystem\Filesystem;

abstract class FileTemplateGenerator {

    protected $file;
    protected $template;

    public function __construct(Filesystem $file){
        $this->file = $file;
    }

    protected  function populateTemplate(array $tokens,array $input){
        foreach($tokens as $token){
            $this->template = str_replace('{{'.$token.'}}',$input[$token],$this->template);
        }
        return $this->template;
    }

    protected  function saveTemplateToFile( $filePath){
        return $this->file->put($filePath, $this->template);
    }

    public  function make($name,$saveTo,array $tokens,array $input){
        $this->populateTemplate($tokens,$input);
        return $this->saveTemplateToFile($saveTo);
    }

    abstract function getTemplate($name);
    abstract function getTokens();

}